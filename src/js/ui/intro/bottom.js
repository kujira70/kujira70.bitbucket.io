let node = document.createElement("div");
node.classList.add("bottom");
node.innerHTML = "BOTTOM";

const TEST = "xxxxxxxxxx";
const PAD = "  ";

const KAGAMI = [
	　"　　　　　　　　　　　　＿＿＿ r -v ､ _＿＿＿            ",
　　"　　　　　　- ﾆ 二_ ` ､_::: -‐`…‐'´- _::::::::::::::7  ",
　　"　　　　　　__ 　-―` 　　　　　　　　　　｀ヽ:::/        ",
　　"　 　 　,. ´　　,. 　´　　　　　　　　　　　　 　＼      ",
　　" 　 ／　 __ ／　　　　　/　 /　　　　　　　　ヽ　ヽ.     ",
　　"　/'´￣ ／'　　 /　　 /　 / / ∧　 |　　　　　∨ ﾊ        ",
　　"　　　　//　 　/　 　/　 ｲ　'　| 　!　ﾄ　＼　　　∨ l     ",
　  "　 　　 /ｲ 　　,′　ｨ ⌒　|　|　| 　!　l　⌒ ヽ 　|　V    ",
　　"　　 〃 |　　│ 　 |　/ 　|　|　| 　 V 　　＼|　 |　　',  ",
　  "　 　 l! 　ﾚ　　|　　 |/_＿　V 　 　 　　 _ ＿|,_　ﾄ､ 　 ,",
　　"　　　　 |　　 |　　/ヾi ￣`r　 　　 　彳´￣ﾊ　ﾚ ヽ　l",
　　"　　　　 |　/|∧ ∧ 　VU`l 　 　　 　 l'ひV　!│ l　ヽ!",
　　"　　　　 ﾚ'　l　 !　ﾊ 　ゝ- ' / / / // ｀　´ ∧.ﾚ′",
　　"　　　　　　　　 V　ゝ　　　　 　 　　 　　 ノ ´ﾚ′",
　　"　　　　　 　　　　 　 　＞ .. _　 ´　_ .. ィ",
　  "　 　 　 　　　 　　 　　_|ノ^､l ￣ l∧　|",
　　"　　 　 　　 　 　ィ´　丁| i ヽヽ_ _// ﾊ￣/ ヽ",
　　"　 　　 　　 　 /│　　V| i　 ゝ--く / ハ′ ハ",
]

// const KNIGHT = [
// 	"   .-.   ",
// 	" __|=|__ ",
// 	"(_/'-'\\_)",
// 	"//\\___/\\\\",
// 	"<>/   \\<>",
// 	" \\|_._|/ ",
// 	"  <_I_>  ",
// 	"   |||   ",
// 	"  /_|_\\  "
// ];

const FLOWER = [
    "     ",
    "     ",
    "     ",
    "     ",
    "     ",
	" .:. ",
	"-=o=-",
	" ':' ",
	" \\|/ "
];

function colorizeKnight(ch) {
	let color = "#aae";
	return `<span style="color:${color}">${ch}</span>`;
}

function colorizeFlower(ch) {
	let color = "#f00";
	if (ch == "o") { color = "#ff0"; }
	if (ch == "\\" || ch == "/" || ch == "|") { color = "lime"; }
	ch = ch.replace(/</, "&lt;").replace(/>/, "&gt;");
	return `<span style="color:${color}">${ch}</span>`;
}

export function fit() {
	let avail = node.parentNode.offsetWidth;
	node.innerHTML = TEST;
	let columns = Math.floor(TEST.length*avail/node.offsetWidth) - 2;

	// let knight = KNIGHT.join("\n").replace(/\S/g, colorizeKnight).split("\n");
	let knight = KAGAMI.join("\n").replace(/\S/g, colorizeKnight).split("\n");
	let flower = FLOWER.join("\n").replace(/\S/g, colorizeFlower).split("\n");

	let result = [];
	for (let i=0;i<knight.length;i++) {
		let remain = columns;
		remain -= PAD.length + 20; // padding
		remain -= 20; // knight
		remain -= 5; // flower

		let row = `${PAD}${knight[i]}${new Array(remain+1).join(" ")}${flower[i]}`;
		result.push(row);
	}

	let final = `<span class='grass'>${new Array(columns+1).join("^")}</span>`;
	result.push(final);

	node.innerHTML = result.join("\n");
}

export function getNode() {
	return node;
}
